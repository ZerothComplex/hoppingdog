﻿using UnityEngine;
using System.Collections.Generic;

public class ObstacleController : MonoBehaviour {

    public float speed = 5f;
    public bool isObstaclesEnable = true;
    public List<GameObject> Obstacles;

    private int first = 0;
    private int current = 0;
    private float startTime = 0;

    void OnEnable ()
    {
        first = Random.Range(0, Obstacles.Count);

        if (isObstaclesEnable)
        {
            for (int i = 0; i < Obstacles.Count; i++)
            {
                Obstacles[i].SetActive(false);

                if (i == first)
                    Obstacles[first].SetActive(true);
            }
        }

        startTime = Time.time;
    }

    void Update ()
    {
           
        if (transform.position.x < -18.37)
        {
            transform.position = new Vector2(10.82f, -0.14f);

            if (isObstaclesEnable)
            {
                current = Random.Range(0, Obstacles.Count);

                for (int i = 0; i < Obstacles.Count; i++)
                {
                    Obstacles[i].SetActive(false);

                    if (i == current)
                        Obstacles[current].SetActive(true);
                }
            }
        }

        if(startTime > 50 && (Time.time - startTime )%50 == 0)
        {
            speed++;
        }

        if (!LevelManager.Instance.isDead)
            transform.Translate(Vector3.left * Time.deltaTime * speed);
        else
        {
            startTime = Time.time;
            speed = 5f;
        }
	}

    public void Reset()
    {
        Debug.Log("Reseting Obstacles");
        transform.position = new Vector2(10.82f, -0.14f);
        current = Random.Range(0, Obstacles.Count);

        for (int i = 0; i < Obstacles.Count; i++)
        {
            Obstacles[i].SetActive(false);

            if (i == current)
                Obstacles[current].SetActive(true);
        }
    }
}
