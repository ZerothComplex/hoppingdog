﻿using UnityEngine;

public class GroundController : MonoBehaviour
{

    public float speed = 5f;

    void Start()
    {
    }

    void Update()
    {

        if (transform.position.x < -199.68)
        {
            transform.position = new Vector2(0f, -0.14f);

        }

            transform.Translate(Vector3.left * Time.deltaTime * speed);

        if (LevelManager.Instance.isDead)
            transform.position = new Vector2(0f, -0.14f);
    }

}
