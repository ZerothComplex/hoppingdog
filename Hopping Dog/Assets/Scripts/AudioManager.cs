﻿using UnityEngine;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour {

    public AudioSource buttonClickAudio;
    public AudioSource mainMenuMusic;
    public AudioSource playerJumpAudio;
    public AudioSource deathAudio;

    public static AudioSource[] as_Sources;

    public static bool isSoundOn = true;

    void Awake()
    {
        as_Sources = gameObject.GetComponents<AudioSource>();
    }

    public static void PlayEffects(string soundState)
    {
        if (PlayerPrefs.GetInt("isSoundOff") == 0)
        {
            switch (soundState)
            {
                case "ButtonClick":
                    as_Sources[0].Play();
                    break;
                case "Music":
                    as_Sources[1].Play();
                    break;
                case "Jump":
                    as_Sources[2].Play();
                    break;
                case "Death":
                    as_Sources[3].Play();
                    break;
                default:
                    break;
            }
        }
    }

    public static void StopEffects(string soundState)
    {
        if (PlayerPrefs.GetInt("isSoundOff") == 0)
        {
            switch (soundState)
            {
                case "Music":
                    as_Sources[1].Stop();
                    break;
                default:
                    break;
            }
        }
    }

    public static void PauseEffects(string soundState)
    {
        switch (soundState)
        {
            case "Music":
                as_Sources[1].Pause();
                break;
            default:
                break;
        }
    }

   

}
